//
//  TwittReaderTests.m
//  TwittReaderTests
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TRTweetListModel.h"

@interface TwittReaderTests : XCTestCase <TRTWeetListModelDelegate>

@property (nonatomic, strong) TRTweetListModel *model;
@property (nonatomic, strong) NSArray *tweets;
@property (nonatomic, strong) XCTestExpectation *expectation;

@end

@implementation TwittReaderTests

- (void)setUp {
    [super setUp];
    self.model = [TRTweetListModel new];
    [self.model setDelegate:self];
}

- (void)tearDown {
    self.model = nil;
    self.tweets = nil;
    self.expectation = nil;
    [super tearDown];
}

- (void)testModelReturnsDataForSimpleSearchString {
    [self.model setSearchText:@"Hello"];
    
    self.expectation = [self expectationWithDescription:@"Model returns data for simple search string"];
    
    [self.model loadMoreData];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error: %@", error);
        }
    }];
}

- (void)testClearSearchInfoClearsSearchString {
    [self.model setSearchText:@"Hello"];
    
    self.expectation = [self expectationWithDescription:@"Model returns data for simple search string"];
    
    [self.model loadMoreData];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            [self.model clearSearchInfo];
            XCTAssert(self.model.searchText == nil);
            XCTAssert([[self.model tweets] count] == 0);
        }
    }];
}

- (void)testCanRefreshDataWorks {
    
    self.model.searchText = nil;
    BOOL canRefresh = [self.model canRefershData];
    XCTAssert(canRefresh == NO);
    
    self.model.searchText = @"";
    canRefresh = [self.model canRefershData];
    XCTAssert(canRefresh == NO);
    
    self.model.searchText = @"Hello";
    canRefresh = [self.model canRefershData];
    XCTAssert(canRefresh == YES);
}

#pragma mark - TRTWeetListModelDelegate

- (void)modelDidUpdateData:(TRTweetListModel *)model {
    self.tweets = model.tweets;
    
    [self.expectation fulfill];
    self.expectation = nil;
}

@end
