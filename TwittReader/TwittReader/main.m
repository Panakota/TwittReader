//
//  main.m
//  TwittReader
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
