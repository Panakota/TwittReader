//
//  TRTweetListTableViewController.h
//  TwittReader
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRTweetListTableViewController : UIViewController <UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (nonatomic) NSUInteger topBarHeight;

@end
