//
//  TRTweetListTableViewController.m
//  TwittReader
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import "TRTweetListTableViewController.h"
#import "TRTweetListModel.h"
#import <TwitterKit/TWTRTweetTableViewCell.h>
#import "UIScrollView+BottomRefreshControl.h"
#import "RealReachability.h"
#import "UIView+Toast.h"

static NSString * const kCellIdentifier = @"TweetCell";

@interface TRTweetListTableViewController () <TRTWeetListModelDelegate>
@property (nonatomic, strong) TRTweetListModel * model;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *topRefreshControl;
@end

@implementation TRTweetListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[TWTRTweetTableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.model = [[TRTweetListModel alloc] init];
    self.model.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIEdgeInsets insets = self.tableView.contentInset;
    insets.top = self.topBarHeight;
    self.tableView.contentInset = insets;
    
    [self setupToast];
    [self setupRefreshControls];
}

#pragma mark - UI Setup

- (void)setupToast {
    [CSToastManager setQueueEnabled:NO];
    [CSToastManager setTapToDismissEnabled:YES];
}

- (void)setupRefreshControls {
    
    // Top Refresh Control
    self.topRefreshControl = [[UIRefreshControl alloc] init];
    self.topRefreshControl.tintColor = [UIColor grayColor];
    [self.topRefreshControl addTarget:self action:@selector(topRefreshControlAction:) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView insertSubview:self.topRefreshControl atIndex:0];
    
    // Bottom Refresh Control
    UIRefreshControl *bottomRefresh = [[UIRefreshControl alloc] init];
    bottomRefresh.triggerVerticalOffset = 100;
    bottomRefresh.tintColor = [UIColor grayColor];
    [bottomRefresh addTarget:self action:@selector(bottomRefreshControlAction:) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = bottomRefresh;
}

- (void)topRefreshControlAction:(id)sender {
    if ([self isInternetReachable] && [self.model canRefershData]) {
        [self.model reloadData];
    } else {
        [self.topRefreshControl endRefreshing];
    }
}

- (void)bottomRefreshControlAction:(id)sender {
    if ([self isInternetReachable] && [self.model canRefershData]) {
        [self.model loadMoreData];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView.bottomRefreshControl endRefreshing];
        });
    }
}

#pragma mark - Reachability

- (BOOL)isInternetReachable {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    if (status == RealStatusUnknown || status == RealStatusNotReachable) {
        [self.view makeToast:@"No Internet Connection"
                                  duration:2.0
                                  position:CSToastPositionCenter];
        return NO;
    }
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.model tweets] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TWTRTweetTableViewCell *cell = (TWTRTweetTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    TWTRTweet *tweet = [self.model tweetForIndex:indexPath.row];
    [cell configureWithTweet:tweet];
        
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    TWTRTweet *tweet = [self.model tweetForIndex:indexPath.row];
    return [TWTRTweetTableViewCell heightForTweet:tweet style:TWTRTweetViewStyleCompact width:tableView.bounds.size.width showingActions:NO];
}

#pragma mark - TRTweetListModel delegate

- (void)modelDidUpdateData:(TRTweetListModel *)model {
    [self.tableView reloadData];
    
    [self.topRefreshControl endRefreshing];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView.bottomRefreshControl endRefreshing];
    });
}

#pragma mark - UISearchResultsUpdatingDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.model clearSearchInfo];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString * text = searchBar.text;
    [self.model setSearchText:text];
    
    if (text && text.length) {
        if ([self isInternetReachable]) {
            [self.model loadMoreData];
        }
    }
}

@end
