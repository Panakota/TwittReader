//
//  TRTweetListModel.m
//  TwittReader
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import "TRTweetListModel.h"

const NSInteger kRequestedTweetsCount = 6;

@interface TRTweetListModel ()

@property (nonatomic, strong) NSMutableArray * tweetsArray;
@property (nonatomic, strong) TWTRAPIClient *apiClient;

@end

@implementation TRTweetListModel

- (id)init {
    self = [super init];
    if (self) {
        _tweetsArray = [[NSMutableArray alloc] init];
        _apiClient = [[TWTRAPIClient alloc] init];
    }
    return self;
}

- (NSArray *)tweets {
    return self.tweetsArray;
}

- (TWTRTweet *)tweetForIndex:(NSInteger)ind {
    if ((ind < 0) || (ind > [self.tweetsArray count] - 1)) {
        return nil;
    }
    return self.tweetsArray[ind];
}

- (void)clearSearchInfo {
    
    self.searchText = nil;
    [self removeData];
}

- (BOOL)canRefershData {
    
    if (!self.searchText || !self.searchText.length) {
        return NO;
    }
    return YES;
}

- (void)loadData {
    [self requestTweetsFromId:0];
}

- (void)reloadData {
    [self removeData];
    [self loadData];
}

- (void)loadMoreData {
    if (!self.searchText || !self.searchText.length) {
        return;
    }
    
    NSInteger oldestTweetId = 0;
    // If some tweets were already loaded
    if ([self.tweetsArray count]) {
        TWTRTweet * oldestTweet = [self.tweetsArray lastObject];
        oldestTweetId = [oldestTweet.tweetID integerValue];
        if (oldestTweetId > 1) {
            oldestTweetId -= 1; // Increment oldestTweetId to load earlier tweets in new request
        }
    }
    [self requestTweetsFromId:oldestTweetId];
}

#pragma mark - Private

- (void)removeData {
    [self.tweetsArray removeAllObjects];
    [self.delegate modelDidUpdateData:self];
}

- (void)requestTweetsFromId:(NSInteger)maxTweetId {
    
    NSString *searchEndpoint = @"https://api.twitter.com/1.1/search/tweets.json";
    
    NSMutableDictionary *params = [@{@"q" : self.searchText, @"count" : [@(kRequestedTweetsCount)stringValue]} mutableCopy];
    if (maxTweetId) {
        [params setObject:[@(maxTweetId) stringValue] forKey:@"max_id"];
    }
    
    NSError *clientError;
    
    NSURLRequest *request = [self.apiClient URLRequestWithMethod:@"GET" URL:searchEndpoint parameters:params error:&clientError];
    
    if (request) {
        [self.apiClient sendTwitterRequest:request completion:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            if (data) {
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                if (jsonError) {
                    NSLog(@"Error: %@", jsonError);
                } else {
                    if ([json objectForKey:@"statuses"]) {
                        NSArray *tweets = [TWTRTweet tweetsWithJSONArray:[json objectForKey:@"statuses"]];
                        [self.tweetsArray addObjectsFromArray:tweets];
                    }
                }
            }
            else {
                NSLog(@"Error: %@", connectionError);
            }
            
            [self.delegate modelDidUpdateData:self];
        }];
    }
    else {
        NSLog(@"Error: %@", clientError);
    }
}

@end
