//
//  TRTweetListModel.h
//  TwittReader
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TwitterKit/TwitterKit.h>

@class TRTweetListModel;

@protocol TRTWeetListModelDelegate <NSObject>

- (void)modelDidUpdateData:(TRTweetListModel *)model;

@end

@interface TRTweetListModel : NSObject

@property (nonatomic, weak) id<TRTWeetListModelDelegate> delegate;

@property (nonatomic, strong) NSString *searchText;

- (NSArray *)tweets;

- (TWTRTweet *)tweetForIndex:(NSInteger)ind;

- (void)clearSearchInfo;

- (BOOL)canRefershData;

// Called When Top 'Pull to refresh' was used
- (void)reloadData;

// Called when tap search button on searchBar's keyboard or bottom 'Pull to refresh' was used
- (void)loadMoreData;

@end
