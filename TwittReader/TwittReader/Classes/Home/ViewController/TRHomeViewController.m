//
//  TRHomeViewController.m
//  TwittReader
//
//  Created by Anastasiia on 12/21/16.
//  Copyright © 2016 Anastasiia. All rights reserved.
//

#import "TRHomeViewController.h"
#import "TRTweetListTableViewController.h"

@interface TRHomeViewController ()

@property (nonatomic, strong) UISearchController * searchController;

@end

@implementation TRHomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupSearchController];
}

#pragma mark - SearchController

- (void)setupSearchController {
    
    TRTweetListTableViewController * searchResultController = [[TRTweetListTableViewController alloc] initWithNibName:@"TRTweetListTableViewController" bundle:nil];
    
    searchResultController.topBarHeight = self.navigationController.navigationBar.bounds.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultController];
    
    self.searchController.searchResultsUpdater = searchResultController;
    self.searchController.dimsBackgroundDuringPresentation = YES;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.delegate = searchResultController;
    self.definesPresentationContext = YES;
    self.navigationItem.titleView = self.searchController.searchBar;
}

@end
